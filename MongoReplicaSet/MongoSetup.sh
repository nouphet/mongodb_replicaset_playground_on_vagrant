#!/usr/bin/env bash

if [ -f "/var/vagrant_provision" ]; then
exit 0
fi

cp /vagrant/mongodb.repo /etc/yum.repos.d/
yum install -y mongo-10gen mongo-10gen-server
echo "192.168.111.101 mongo1" >> /etc/hosts
echo "192.168.111.102 mongo2" >> /etc/hosts
echo "192.168.111.103 mongo3" >> /etc/hosts
echo "replSet = rs0" >> /etc/mongod.conf
service mongod start
service iptables stop        
chkconfig mongod on
chkconfig iptables off

touch /var/vagrant_provision
